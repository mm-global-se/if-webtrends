mmcore.IntegrationFactory.register('WebTrends', {
  defaults: {
    isStopOnDocEnd: true
  },

  validate: function (data) {
    if(!data.campaign)
        return 'No campaign.';

    return true;
  },

  check: function (data) {
    return typeof window.dcsMultiTrack === 'function';
  },

  exec: function (data) {
    var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_';
    dcsMultiTrack('DCS.dcsuri', document.location.pathname, 'WT.ti', document.title,'WT.dl', '99','DCSext.dcsmvt', mode + data.campaignInfo);
    
    if(data.callback) data.callback();
    return true;
  }
});