# WebTrends

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __WebTrends__.

## How we send the data

We are calling `window.dcsMultiTrack()` function passing campaign experience along with other parameters (such as `document.location.pathname`, `document.location.title`) according to Webtrends integration documentation.

_Example_:

```javascript
dcsMultiTrack('DCS.dcsuri', document.location.pathname, 'WT.ti', document.title,'WT.dl', '99','DCSext.dcsmvt', 'MM_Prod_T1Button=color:red');
```

## Data Format

The data sent to Kissmetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [webtrends-register.js](https://bitbucket.org/mm-global-se/if-webtrends/src/master/src/webtrends-register.js)

* [webtrends-initialize.js](https://bitbucket.org/mm-global-se/if-webtrends/src/master/src/webtrends-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [webtrends-register.js](https://bitbucket.org/mm-global-se/if-webtrends/src/master/src/webtrends-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [webtrends-initialize.js](https://bitbucket.org/mm-global-se/if-webtrends/src/master/src/webtrends-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

### Install Chrome Extension

  + Install [ObservePoint](https://chrome.google.com/webstore/detail/observepoint-tag-debugger/daejfbkjipkgidckemjjafiomfeabemo) Chrome plugin

  + Restart browser

  + New tab will appear in Chrome DevTools

### Test

  + Open ObservePoint tab

  + Reload test page

  + Check for `dcsmvt` variable in Variables tab

![2015-04-29_1437.png](https://bitbucket.org/repo/jyeMyo/images/55266802-2015-04-29_1437.png)